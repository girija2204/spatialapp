from django.urls import path
from frontend.views import showlist

urlpatterns = [
    path('config', showlist, name='config'),
]
