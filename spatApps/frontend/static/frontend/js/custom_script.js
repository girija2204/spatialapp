window.onload = function() {
    buildlist()

    function buildlist(){
        var wrapper = document.getElementById('list_table')
        var url = 'http://127.0.0.1:8000/sentinel_app/properties'
        fetch(url)
        .then((resp) => resp.json())
        .then(function(data){
            var list = data
            for (var i in list){
                var item = `<tr role="row">
                                <td style=" visibility: hidden; ">${list[i].id}</td>
                                <td>${list[i].property_name}</td>
                                <td class="_myColumn">${list[i].property_value}</td>
                            </tr>`
                wrapper.innerHTML += item
            }
        })
    }

    $(document).on('dblclick','._myColumn',function(){
        var value = $(this).text();
        var input = "<input type='text' class='input-data' value='"+value+"' class='form-control'>"
        $(this).html(input);
        $(this).remove("_myColumn");
    });

    $(document).on('blur','.input-data',function(){
        var value = $(this).val();
        var td = $(this).parent("td");
        $(this).remove();
        console.log('value: ',value);
        td.html(value);
        var tr = td.closest('tr')
        id = td.closest('tr').children()[0].firstChild.data
        sendToServer(id, value, td.prev().text())
    });
    $(document).on('keypress','.input-data',function(e){
        var key = e.which;
        if(key == 13){
            var value = $(this).val();
            var td = $(this).parent("td");
            $(this).remove();
            console.log(value);
            td.html(value);
            td.addClass("_myColumn");
            var tr = td.closest('tr')
            id = td.closest('tr').children()[0].firstChild.data
            sendToServer(id, value, td.prev().text())
        }
    });
    function sendToServer(id, data, prev){
        console.log(id);
        console.log(data);
        console.log(prev);
        $.ajax({
            url: 'http://127.0.0.1:8000/sentinel_app/properties',
            type: 'PUT',
//            dataType: "json",
//            contentType : "application/json",
            data: {id:id,property_value:data,property_name:prev}
        });
    }
}