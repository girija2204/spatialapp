from django.urls import path
from sentinel_app.views import PropertiesList, BooksList, AuthorList, GenreList, AuthorView, GenreView, BookView

urlpatterns = [
    path('properties', PropertiesList.as_view(), name='properties'),
    path('books', BooksList.as_view(), name='books'),
    path('authors', AuthorList.as_view(), name='authors'),
    path('genres', GenreList.as_view(), name='genres'),
    path('authors/<pk>/books', AuthorView.as_view(), name='author-detail'),
    path('genres/<pk>', GenreView.as_view(), name='genre-detail'),
    path('books/<pk>', BookView.as_view(), name='book-detail')
]
