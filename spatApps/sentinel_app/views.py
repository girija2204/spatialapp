from .models import AppPropertyModel, Author, Genre, Book
from .serializer import AppPropertyModel_Serializer, AuthorModel_Serializer, GenreModel_Serializer, \
    BookModel_Serializer, BookSerializer, AuthorSerializer, GenreSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
import pdb


class PropertiesList(APIView):
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        id = self.request.query_params.get('id')
        querySet = AppPropertyModel.objects.all()
        if id:
            querySet = querySet.filter(id=id)
        return querySet

    def get(self, request):
        print(f'hellohi')
        appProperties = AppPropertyModel.objects.all().order_by('id')
        serializer = AppPropertyModel_Serializer(appProperties, many=True)
        return Response(serializer.data)

    def put(self, request):
        print(f'data: {request.data}')
        property_toupdate = AppPropertyModel.objects.get(pk=request.data['id'])
        serializer = AppPropertyModel_Serializer(property_toupdate, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PropertyDetail(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, pk):
        print(f'hello')
        appProperty = AppPropertyModel.objects.get(pk=pk)
        serializer = AppPropertyModel_Serializer(appProperty)
        return Response(serializer.data)


class BooksList(APIView):
    renderer_classes = [JSONRenderer]

    @method_decorator(cache_page(60*60*2))
    def get(self, request):
        books = Book.objects.all()
        # serializer = BookModel_Serializer(books, many=True, context={'request': request})
        serializer = BookSerializer(books, many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request):
        pdb.set_trace()
        serializer = BookSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BookView(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, **kwargs):
        book = Book.objects.get(id=kwargs['pk'])
        # serializer = BookModel_Serializer(book, context={'request': request})
        serializer = BookSerializer(book, context={'request': request})
        return Response(serializer.data)

    def put(self, request, **kwargs):
        pdb.set_trace()
        book = Book.objects.get(id=kwargs['pk'])
        # serializer = BookModel_Serializer(book, data=request.data, context={'request': request})
        serializer = BookSerializer(book)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, **kwargs):
        book = Book.objects.get(id=kwargs['pk'])
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AuthorList(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        authors = Author.objects.all()
        # serializer = AuthorModel_Serializer(authors, many=True, context={'request': request})
        serializer = AuthorSerializer(authors, many=True)
        return Response(serializer.data)

    def post(self, request):
        pdb.set_trace()
        # serializer = AuthorModel_Serializer(data=request.data, context={'request': request})
        serializer = AuthorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthorView(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, **kwargs):
        pdb.set_trace()
        author = Author.objects.get(id=kwargs['pk'])
        # serializer = AuthorModel_Serializer(author, context={'request': request})
        serializer = AuthorSerializer(author, context={'request': request})
        return Response(serializer.data)

    def put(self, request, **kwargs):
        author = Author.objects.get(id=kwargs['pk'])
        serializer = AuthorSerializer(author, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, **kwargs):
        author = Author.objects.get(id=kwargs['pk'])
        author.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GenreList(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        genres = Genre.objects.all()
        # serializer = GenreModel_Serializer(genres, many=True, context={'request': request})
        serializer = GenreSerializer(genres, many=True)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        # serializer = GenreModel_Serializer(data=request.data, context={'request': request})
        serializer = GenreSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GenreView(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request, **kwargs):
        genre = Genre.objects.get(id=kwargs['pk'])
        serializer = GenreSerializer(genre, context={'request': request})
        # serializer = GenreModel_Serializer(genre, context={'request': request})
        return Response(serializer.data)

    def put(self, request, **kwargs):
        genre = Genre.objects.get(id=kwargs['pk'])
        serializer = GenreSerializer(genre, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, **kwargs):
        genre = Genre.objects.get(id=kwargs['pk'])
        genre.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)