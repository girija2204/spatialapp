from django.contrib.gis import admin
from .models import AppPropertyModel, Book, Author, Genre

# Register your models here.
admin.register(AppPropertyModel, Book, Author, Genre)