from rest_framework import serializers
from .models import AppPropertyModel, Author, Genre, Book
import pdb


class AppPropertyModel_Serializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = AppPropertyModel
        fields = ['id', 'property_name', 'property_value']


class BookDetailSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    genres = serializers.StringRelatedField(many=True)

    class Meta:
        model = Book
        fields = ['url', 'id', 'name', 'genres']


class AuthorModel_Serializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    # books = serializers.StringRelatedField(many=True)
    # books = serializers.HyperlinkedRelatedField(view_name='book-detail', read_only=True, many=True)
    books = BookDetailSerializer(many=True, required=False)

    def create(self, validated_data):
        pdb.set_trace()
        return Author.objects.create(**validated_data)

    class Meta:
        model = Author
        fields = ['url', 'id', 'name', 'age', 'books']


class GenreBooksSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    author = serializers.StringRelatedField()

    class Meta:
        model = Genre
        fields = ['url', 'id', 'name', 'author']


class GenreModel_Serializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    # books = serializers.StringRelatedField(many=True)
    # books = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='book-detail')
    books = GenreBooksSerializer(many=True, required=False)

    def create(self, validated_data):
        return Genre.objects.create(**validated_data)

    class Meta:
        model = Genre
        fields = ['url', 'id', 'name', 'books']


class AuthorDetail_Serializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Author
        fields = ['url', 'id', 'name', 'age']


class GenreDetailSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Genre
        fields = ['url', 'id', 'name']


class BookModel_Serializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    # author = serializers.StringRelatedField()
    # author = serializers.HyperlinkedRelatedField(view_name='author-detail',read_only=True)
    # author = AuthorModel_Serializer()
    author = AuthorDetail_Serializer(read_only=True)
    # genres = serializers.StringRelatedField(many=True)
    # genres = serializers.HyperlinkedRelatedField(view_name='genre-detail',read_only=True,many=True)
    # genres = GenreModel_Serializer(many=True)
    genres = GenreDetailSerializer(many=True)

    def create(self, validated_data):
        author_data = validated_data.pop('author', None)
        genres_data = validated_data.pop('genres', None)
        if author_data:
            author = Author.objects.get(**author_data)[0]
            validated_data['author'] = author
        book = Book.objects.create(**validated_data)
        if genres_data:
            for genre_data in genres_data:
                genre = Genre.objects.get(**genre_data)
                book.genres.add(genre)
                book.save()
        return book

    def update(self, instance, validated_data):
        pdb.set_trace()
        instance.name = validated_data.get('name', instance.name)
        instance.price = validated_data.get('price', instance.price)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        genres_data = validated_data.get('genres', instance.genres)
        if genres_data:
            for genre_data in genres_data:
                genre = Genre.objects.get(**genre_data)
                instance.genres.add(genre)
                instance.save()
        return instance

    class Meta:
        model = Book
        fields = ['url', 'id', 'name', 'price', 'description', 'author', 'genres']


# For article
class RelatedBooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['name', 'description', 'url']


class AuthorSerializer(serializers.ModelSerializer):
    books = RelatedBooksSerializer(required=False,many=True)

    def create(self, validated_data):
        pdb.set_trace()
        return Author.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.age = validated_data.get('age', instance.age)
        instance.save()
        return instance

    class Meta:
        model = Author
        fields = ['id', 'name', 'age', 'books']


class RelatedAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['name', 'url', 'age']


class RelatedGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ['name', 'url']


class BookSerializer(serializers.ModelSerializer):
    author = RelatedAuthorSerializer(required=False)
    genres = RelatedGenreSerializer(required=False, many=True)

    def create(self, validated_data):
        pdb.set_trace()
        author_data = validated_data.pop('author', None)
        genres_data = validated_data.pop('genres', None)
        if author_data:
            validated_data['author'] = Author.objects.get(**author_data)
        book = Book.objects.create(**validated_data)
        if genres_data:
            for genre_data in genres_data:
                book.genres.add(Genre.objects.get(**genre_data))
                # book.genres.add(genre_data)
                book.save()
        return book

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.price = validated_data.get('price', instance.price)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance

    class Meta:
        model = Book
        fields = ['id', 'name', 'url', 'description', 'price', 'author', 'genres']


class GenreSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        return Genre.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance

    class Meta:
        model = Genre
        fields = ['id', 'name']
