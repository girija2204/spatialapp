from django.db.models import Model, CharField, DateTimeField, IntegerField, ForeignKey, CASCADE, ManyToManyField


class BaseModel(Model):
    inserted_on: DateTimeField = DateTimeField(auto_now=True)
    modified_on: DateTimeField = DateTimeField(auto_now=True)
    version: IntegerField = IntegerField(default=1, auto_created=True)

    class Meta:
        abstract = True


class AppPropertyModel(BaseModel):
    property_name: CharField = CharField(max_length=100)
    property_value: CharField = CharField(max_length=100)


class Author(BaseModel):
    name = CharField(max_length=100)
    age = IntegerField()

    class Meta:
        ordering = ['name']

    def __str__(self):
        return f'{self.name}'


class Genre(BaseModel):
    name = CharField(max_length=20)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return f'{self.name}'


class Book(BaseModel):
    name = CharField(max_length=100)
    price = IntegerField()
    description = CharField(max_length=200)
    author = ForeignKey(Author, on_delete=CASCADE, related_name='books')
    genres = ManyToManyField(Genre, related_name='books')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return f'name: {self.name}, price: {self.price}'
