from django.apps import AppConfig


class SentinelAppConfig(AppConfig):
    name = 'sentinel_app'
