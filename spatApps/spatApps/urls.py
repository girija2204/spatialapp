from django.contrib.gis import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path("sentinel_app/", include("sentinel_app.urls")),
    path("", include("frontend.urls")),
    path("footprint_manager/", include("footprint_manager.urls"))
]
