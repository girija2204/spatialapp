from django.apps import AppConfig


class FootprintManagerConfig(AppConfig):
    name = 'footprint_manager'
