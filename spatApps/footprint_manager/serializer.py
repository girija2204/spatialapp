from rest_framework import serializers
from footprint_manager.models import GeoLocation, ShapeLocator
import pdb


class ShapeLocatorSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        # pdb.set_trace()
        return ShapeLocator.objects.create(**validated_data)

    class Meta:
        model = ShapeLocator
        fields = ['url', 'id', 'file_name', 'file_location', 'file_type']


class FootprintSerializer(serializers.ModelSerializer):
    shape_locator = ShapeLocatorSerializer()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        # pdb.set_trace()
        validated_data['shape_locator'] = ShapeLocator.objects.filter(**validated_data['shape_locator'])[0]
        return GeoLocation.objects.create(**validated_data)

    class Meta:
        model = GeoLocation
        fields = ['url', 'id', 'location_name', 'type', 'shape_locator']
