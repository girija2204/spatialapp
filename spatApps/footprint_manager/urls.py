from django.urls import path
from footprint_manager.views import FootprintsList, ShapeLocatorsList, FootprintDetail, ShapeLocatorDetail


urlpatterns = [
    path('footprints', FootprintsList.as_view(), name='footprints_list'),
    path('shapelocators', ShapeLocatorsList.as_view(), name='shapelocators_list'),
    path('footprints/<pk>', FootprintDetail.as_view(), name='geolocation-detail'),
    path('shapelocators/<pk>', ShapeLocatorDetail.as_view(), name='shapelocator-detail')
]
