from django.contrib import admin
from footprint_manager.models import GeoLocation, ShapeLocator

# Register your models here.
admin.register(GeoLocation, ShapeLocator)
