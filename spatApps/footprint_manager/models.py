from sentinel_app.models import BaseModel
from django.db.models import CharField, IntegerField, ForeignKey, CASCADE, BooleanField


class ShapeLocator(BaseModel):
    file_name = CharField(max_length=100, null=False)
    file_location = CharField(max_length=100, null=False)
    file_type = IntegerField(null=False)

    def __str__(self):
        return f'file name: {self.file_name}, file location: {self.file_location}'


class GeoLocation(BaseModel):
    location_name = CharField(max_length=100, null=False)
    shape_locator = ForeignKey(ShapeLocator, on_delete=CASCADE, related_name='geolocations', null=False)
    type = IntegerField(null=False)
    selected = BooleanField(null=True)

    class Meta:
        ordering = ['location_name']

    def __str__(self):
        return f'name: {self.location_name}, type: {self.type}, file: {self.shape_locator}'
