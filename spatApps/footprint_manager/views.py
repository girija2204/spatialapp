import os
import zipfile
from typing import List, Tuple, Union, Any, Dict

import fiona
from django.conf import settings
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from footprint_manager.models import GeoLocation, ShapeLocator
from footprint_manager.serializer import FootprintSerializer, ShapeLocatorSerializer

base_dir = settings.BASE_DIR
shapefile_zipped_directory = os.path.dirname(os.path.dirname(base_dir)) + '\\resources\\geolocations\\shapefile' \
                                                                          '\\zipped\\ '
shapefile_unzipped_directory = os.path.dirname(os.path.dirname(base_dir)) + '\\resources\\geolocations\\shapefile' \
                                                                            '\\unzipped\\ '


# Create your views here.
class ShapeLocatorsList(APIView):
    parser_classes = [FileUploadParser, ]

    def get(self, request):
        shapeLocators = ShapeLocator.objects.all()
        serializer = ShapeLocatorSerializer(shapeLocators, many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request):
        uploaded_file = request.data['file']
        destination = open(shapefile_zipped_directory + uploaded_file.name, 'wb+')
        for chunk in uploaded_file.chunks():
            destination.write(chunk)

        destination.close()
        serializer = ShapeLocatorSerializer(data={'file_name': uploaded_file.name, 'file_location': shapefile_zipped_directory, 'file_type': 1}, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            with zipfile.ZipFile(shapefile_zipped_directory + uploaded_file.name) as zip_ref:
                zip_ref.extractall(shapefile_unzipped_directory)
                folder_name = shapefile_unzipped_directory + zip_ref.infolist()[0].filename
                file_name = os.path.splitext(uploaded_file.name)[0] + ".shp"
                with fiona.open(folder_name + "\\" + file_name, 'r') as shapefile:
                    feature_list = []
                    i = 0
                    if len(shapefile) > 10:
                        for feature in shapefile:
                            if i <= 10:
                                feature_list.append(feature['properties'])
                                i = i + 1
                    else:
                        for feature in shapefile:
                            feature_list.append(feature['properties'])
            context={
                'data': feature_list,
                'folder_name': folder_name,
                'file_name': file_name,
                'url': reverse('footprints_list', request=request),
                'serializer': serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def add_footprint(shapefile,request) -> Tuple[Union[int, Any], List[Dict[str, Any]]]:
    count = 0
    errors = []
    for feature in shapefile:
        footprint_serializer = FootprintSerializer(
            data={'location_name': feature['properties'][request.data['column_name']], 'type': 1,
                  'shape_locator': request.data['serializer']}, context={'request': request})
        if footprint_serializer.is_valid():
            footprint_serializer.save()
            count = count + 1
        else:
            errors.append({
                "row number": count,
                "data": feature['properties'],
                "reason": footprint_serializer.errors})
    return count, errors


class FootprintsList(APIView):

    def get(self, request):
        footprints = GeoLocation.objects.all()
        footprints_serializer = FootprintSerializer(footprints, many=True, context={'request': request})
        return Response(footprints_serializer.data)

    def post(self, request):
        context = {}
        with fiona.open(request.data['folder_name'] + "\\" + request.data['file_name'], 'r') as shapefile:
            count, errors = add_footprint(shapefile, request)
            context['count'] = count
            context['number of errors'] = len(errors)
            context['errors'] = errors
            context['url'] = reverse('footprints_list', request=request)
        shapefile.close()
        return Response(context, status=status.HTTP_200_OK)


class FootprintDetail(APIView):

    def get(self, request, **kwargs):
        footprint = GeoLocation.objects.get(id=kwargs['pk'])
        serializer = FootprintSerializer(footprint, context={'request': request})
        return Response(serializer.data)


class ShapeLocatorDetail(APIView):

    def get(self, request, **kwargs):
        shapeLocator = ShapeLocator.objects.get(id=kwargs['pk'])
        serializer = ShapeLocatorSerializer(shapeLocator, context={'request': request})
        return Response(serializer.data)
